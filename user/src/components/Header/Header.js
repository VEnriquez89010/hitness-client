import React from 'react';
import './Header.scss?v=2.0.0';
import Logo from './logo.png';
import ls from 'local-storage'
import { OverflowMenu, OverflowMenuItem } from 'carbon-components-react';
import { Link } from 'react-router-dom';

class Header extends React.Component {
    
        render() {
            const renderAuthButton = ()=>{
                let session = ls.get('session');
                let name = ls.get('name');
                if(session == null){
                    return <Link element={Link} to="/login" className="logIn" >Iniciar sesión</Link> 
                } else {
                    return (
                        <>
                        <div className="sessionControl">
                        <Link element={Link} to="/profile" className="userName">
                            <strong>{name}</strong> 
                        </Link>
                        <OverflowMenu className="menu" flipped> 
                            <OverflowMenuItem 
                            itemText="Mi perfil" 
                            primaryFocus
                            onClick={ function profile () {
                                window.location.href='/profile';
                            }}
                            />
                            <OverflowMenuItem
                                itemText="Cerrar sesión"
                                direction='bottom'
                                onClick={ function logout () {
                                    window.location.href='/logout';
                                }}
                                />
                        </OverflowMenu>
                        </div>
                        </>
                    )
                }
            }

            

            return (
                <>
                
                <Link element={Link} to="/">
                    <img src={Logo} alt='website logo' className="logo" />
                </Link>
                {renderAuthButton()}
                <div className="perro"></div>
                <div className="navbarBackground"></div>
                </>
            )
        }
}
export default Header;
