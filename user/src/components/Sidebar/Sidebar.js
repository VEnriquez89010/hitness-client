import React from "react";
import './Sidebar.scss';
import Sidebar from "react-sidebar";
import { Link } from 'react-router-dom';
import ls from 'local-storage'



class SidebarRooeda extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sidebarOpen: false,
    };
    this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
  }

  onSetSidebarOpen(open) {
    document.body.style.overflow =  (open) ? "hidden" : "";
    this.setState({ sidebarOpen: open });
  }
  

  render() {
    const renderAuthButton = ()=>{
      let session = ls.get('session');
      if(session == null){
        return <Link element={Link} to="/login">Iniciar sesión</Link> 
      } else{
        return <Link element={Link} to="/logout">Cerrar sesión</Link> 
      }
    }

    return (
      <Sidebar
        sidebar={
            <div className="SidebarContainer">
            <ul className="Sidebar">
                <li><Link element={Link} to="/studio">Studio</Link></li>
                <li><Link element={Link} to="/instructors">Instructores</Link></li>
                <li><Link element={Link} to="/packs">Clases</Link></li>
                <li><Link element={Link} to="/calendar">Calendario</Link></li>
                <div className="alignBottom" data-type='sideBar'>
                <li className="rooedaSidebarLoginControl">
                  {renderAuthButton()}
                </li>
            </div>
            </ul>

            </div>
        }
        open={this.state.sidebarOpen}
        onSetOpen={this.onSetSidebarOpen}
        shadow={false}
        styles={{ 
            root: { },
            sidebar: { 
                position: "absolute",
                background: "rgba(10,10,12, 0.80)", 
                backdropFilter: "saturate(180%) blur(20px)",
                WebkitBackdropFilter: "saturate(180%) blur(20px)",
                paddingTop: "25pt",
                paddingLeft: "0pt",
                width: "25wv",
                zIndex: 999999,
                transition: "transform .2s ease-out",
                WebkitTransition: "-webkit-transform .2s ease-out"
            },
            content: { overflowY: 'auto' },
            overlay: {
                backgroundColor: "rgba(10,10,12, 0.40)",
                overflow: "hidden",
                zIndex: 999,
                transition: "opacity .1s ease-out, visibility .1s ease-out",
            }
        }}
        >
        <div className="burguerMenu" 
             onClick={() => this.onSetSidebarOpen(true)} 
        >
        </div>
      </Sidebar>
    );
  }
}

export default SidebarRooeda;
