import React from 'react';
import './TermsAndConditions.scss';
import Footer from '../../components/Footer';


const TermsAndConditions = () => {
  return (
    <>
    <div className="studioHero">
      <div className="bx--grid">

      <div className="bx--row">
          <div className="bx--col-sm-8 bx--col-lg-6">
            <h2 className="studioCopy">
              Condiciones de uso.
            </h2>
          </div>      
        </div>          

        <div className="bx--row">
          <div className="bx--col-sm-8 bx--col-lg-6">
            <p>
            Los términos y condiciones descritos en el presente serán aplicables para el uso 
            del sitio web www.rooeda.com y en particular, para utilizar y recibir los Servicios.
            <br></br>
            <br></br>
            <span>
            LE SOLICITAMOS QUE ANTES DE UTILIZAR NUESTROS SERVICIOS, LEA LOS PRESENTES 
            TÉRMINOS Y CONDICIONES.
            </span>
            <br></br>
            <br></br>
            Olga Germaine Parada Gaytan “Rooeda Studio”, como persona física con actividad 
            empresarial, debidamente constituida y válidamente existente de conformidad con 
            las leyes de los Estados Unidos Mexicanos, inscrita en el Registro Público de 
            Comercio de Cd. Obregón, Sonora, Notario Público numero 40, con ejercicio en 
            este distrito y residencia en esta ciudad. 
            <br></br>
            <br></br>
            El domicilio de Rooeda Studio para 
            todo lo relativo a este Contrato es el ubicado en Morelos y Sinaloa #355 
            Plaza Punto Norte <strong>Wellness Center</strong> local 5.

            <br></br>
            <br></br>
            <strong>
            <span>SERVICIOS QUE PRESTA ROOEDA</span>
            </strong>
            <br></br>
            <br></br>

            Rooeda ofrece a sus usuarios y clientes servicios consistentes en ejercicios de 
            alto rendimiento, en específico, ejercicios físicos mediante el uso de bicicletas 
            fijas en las clases, horarios y local de Rooeda y en servicios relacionados a 
            dichas clases, incluyendo de asesoría en materia de acondicionamiento físico 
            (“Servicio”).

            <br></br>
            <br></br>
            <strong>
            <span>¿CÓMO SE SUSCRIBE UN CONTRATO ENTRE ROOEDA Y USTED?</span>
            </strong>
            <br></br>
            <br></br>

            Para poder utilizar los Servicios, usted deberá de ingresar al Sitio Web y solicitar 
            en el mismo el alta de su cuenta de usuario personal, siguiendo las instrucciones y 
            proporcionando la información en el propio Sitio Web requerida. También deberá aceptar 
            los presentes Términos y Condiciones mediante la opción electrónica de aceptación que 
            aparece en el Sitio Web, y dicha aceptación se entenderá como el consentimiento expreso 
            entre usted y ROOEDA para celebrar el presente contrato y cumplir las obligaciones en 
            el mismo establecidas (“Contrato”). Al momento en que usted acepte los presentes 
            Términos y Condiciones, ROOEDA autorizará el alta de su cuenta de usuario personal. 
            Asimismo, usted se acepta y se obliga a entregar a ROOEDA la información personal que 
            ROOEDA considere, en esta fecha o en fecha posterior, necesaria o conveniente para poder 
            brindar adecuadamente los Servicios. Sus datos personales serán tratados conforme al 
            Aviso de Privacidad de ROOEDA, mismo que se transcribe más adelante, y en cumplimiento a 
            lo dispuesto por la Ley Federal de Protección de Datos Personales en Posesión de los 
            Particulares. Mediante la aceptación de los presente Términos y Condiciones y del 
            Aviso de Privacidad, usted acepta proporcionar los datos de su tarjeta de crédito u otro 
            medio de pago electrónico que ROOEDA acepte y manifiesta su aprobación para que ROOEDA 
            realice los cargos correspondientes por los Servicios contratados. Tras completar 
            adecuadamente el registro con ROOEDA, ROOEDA le proporcionará una cuenta personal a la 
            cual usted podrá acceder a través del Sitio Web al ingresar con su nombre de usuario y 
            la contraseña que usted elija. Usted es el único responsable del manejo de su contraseña y 
            de mantenerla confidencial. Usted acepta y reconoce que las autorizaciones, contrataciones 
            de Servicios y consentimientos que se realicen utilizando su nombre de usuario y contraseña 
            se entenderán hechas por usted en lo personal y como consentimiento expreso de su parte. 
            Por lo anterior le recomendamos no revelar a persona alguna su nombre de usuario y/o 
            contraseña. ROOEDA no será responsable por el uso inadecuado, o los cargos que se realicen 
            en su tarjeta de crédito u otro medio de pago que haya proporcionado a través del uso de 
            su nombre de usuario y contraseña.

            <br></br>
            <br></br>
            <strong>
            <span>INSCRIPCIÓN A CLASES DE ROOEDA</span>
            </strong>
            <br></br>
            <br></br>


            Al ingresar al Sitio Web con su cuenta de usuario y contraseña, ROOEDA le permitirá apartar
            un lugar dentro de la clase que usted seleccione de ROOEDA que se encuentre disponible en el sistema, al momento en que usted indique qué clase tomar, usted estará en la lista de asistentes a dicha clase, o bien, en caso de que no haya cupo de dicha clase seleccionada, usted podrá registrarse en la lista de espera y ROOEDA le notificará a su correo electrónico en caso de que algún lugar haya sido desocupado.
            En el momento en que usted indique y solicite se le aparte un cupo dentro de dicha clase,

            <br></br>
            <br></br>
            <strong>
            <span>USO DEL SITIO WEB Y/O DE LOS SERVICIOS</span>
            </strong>
            <br></br>
            <br></br>

            Usted deberá garantizar que la información proporcionada a ROOEDA es completa y veraz. ROOEDA tendrá derecho, en todo momento, a comprobar la veracidad de la información proporcionada.
            Es su responsabilidad asegurarse de ingresar al Sitio Web correcto. ROOEDA no será responsable si no ingresa al Sitio Web correcto. ROOEDA se reserva el derecho a limitar, restringir o incluso prohibir su ingreso al sistema del Sitio Web con su nombre de usuario y contraseña al Sitio Web. 
            <br></br>
            <br></br>
            Asimismo, ROOEDA se reserva el derecho de limitar, restringir o prohibir en cualquier momento que usted utilice sus Servicios, por cualquier causa y sin necesidad de justificarlo.
            Al utilizar el Sitio Web o los Servicios, que usted acuerda y consiente que:
            <br></br>
            <br></br>
            <br></br>
            <ol>
              <li> Sólo utilizará los Servicios y el Sitio Web exclusivamente de manera personal, por lo que no podrá ceder sus espacios en clases u otros Servicios en favor de terceros;</li>
              <li> No autorizará a otros a usar su cuenta;</li>
              <li> No cederá ni transferirá de otro modo su cuenta o derechos bajo este contrato a ninguna otra persona o identidad legal;</li>
              <li> No utilizará una cuenta que esté sujeta a cualquier derecho de una persona que no sea usted, sin la autorización adecuada;</li>
              <li> No utilizará los Servicios o el Sitio Web con fines ilícitos, incluyendo, sin limitación, para enviar o almacenar ningún material ilegal o con fines fraudulentos;</li>
              <li> No utilizará los Servicios o el Sitio Web para causar molestias, trastornos o inconvenientes;</li>
              <li> No perjudicará el funcionamiento adecuado del Sitio Web;</li>
              <li> No tratará de dañar los Servicios o el Sitio Web de ningún modo;</li>
              <li> No copiará ni distribuirá ningún contenido o Servicios de ROOEDA sin la autorización por escrito por ROOEDA;</li>
              <li> Guardará de forma segura y confidencial la contraseña de su cuenta y cualquier información o identificación proporcionada para permitirle acceder al Servicio y al Sitio Web;</li>
              <li> Proporcionará a ROOEDA toda las pruebas de identidad que le solicitemos razonablemente de tiempo en tiempo;</li>
              <li> Cumplirá con toda la legislación y normas aplicables al usar el Sitio Web o los Servicios;</li>
              <li> Mantendrá una buena conducta y será respetuoso con las demás personas que utilicen los Servicios;</li>
              <li> Se obliga a respetar y a dar cumplimiento a los reglamentos que ROOEDA emita en relación con los Servicios;</li>
            </ol>            

            <br></br>
            <br></br>
            ROOEDA se reserva el derecho a terminar, en cualquier momento y de manera inmediata, sin necesidad de declaración judicial los Servicios objeto de las presentes Términos y Condiciones, en caso de que el Usuario incumpla con cualesquiera de las normas anteriores.
            <br></br>
            <br></br>
            <strong>
              <span>PAGO</span>
            </strong>
            <br></br>
            <br></br>
            El uso del Sitio Web será gratuito. La información sobre las tarifas aplicables para los Servicios, las podrá encontrar en el Sitio Web (www.rooeda.com). ROOEDA podrá modificar o actualizar dichas tarifas ocasionalmente, sin necesidad de previo aviso. Será responsabilidad del Usuario mantenerse informado sobre las tarifas actuales para los Servicios.
            ROOEDA le cobrará por los Servicios. Usted acuerda pagar todos los Servicios que solicite (con independencia de si los utiliza o asiste a las clases), mediante cargo automático a tarjeta de crédito o medio de pago electrónico que usted haya proporcionado a través del Sitio Web. El costo de los Servicios causa Impuesto al Valor Agregado. En todo caso, usted será responsable del pago puntual de todos los Servicios que solicite. Los pagos y cargos realizados no son reembolsables.
            ROOEDA podrá utilizar procesadores de pagos de terceros (“Procesador de Pago”) para vincular su tarjeta de crédito o el medio de pago electrónico que nos proporcione a su nombre de usuario y contraseña en el Sitio Web. El procesamiento de pagos, con respecto al uso que haga de los Servicios estará sujeto a las condiciones y políticas de privacidad del Procesador del Pago y el emisor de su tarjeta de crédito, así como a estos Términos y Condiciones. ROOEDA no será responsable de ningún error del Procesador de Pago. En relación con el uso del Sitio Web y los Servicios, ROOEDA obtendrá determinados datos de la transacción, que ROOEDA utilizará únicamente de conformidad al Aviso de Privacidad de ROOEDA.

            <br></br>
            <br></br>
            <strong>
              <span>INDEMNIZACIÓN</span>
            </strong>
            <br></br>
            <br></br>
            Al aceptar estos Términos y Condiciones y utilizar su nombre de usuario y contraseña en el Sitio Web, usted acuerda y acepta que libera a ROOEDA de toda y cualquier responsabilidad, y se obliga a indemnizar y mantener indemne a ROOEDA, sus filiales, licenciatarios y todos sus directivos, instructores, accionistas, otros usuarios, trabajadores, representantes y asesores por cualesquiera reclamaciones, costos, daños, pérdidas, responsabilidades y gastos (incluyendo honorarios y gastos de abogados) derivados de o en relación con:
            ¥ Cualquier violación o incumplimiento a cualquier disposición establecida en estos Términos y Condiciones o cualquier ley o reglamento aplicable, se haga o no referencia al mismo en estos Términos y Condiciones;
            ¥ Cualquier violación de cualquier derecho de cualquier tercero; y/o
            ¥ El uso o uso incorrecto del Sitio Web y/o de los Servicios.

            <br></br>
            <br></br>
            <strong>
              <span>RESPONSABILIDAD</span>
            </strong>
            <br></br>
            <br></br>
            La información, recomendaciones u otros servicios prestados en o a través del Sitio Web y el uso de los Servicios, son sólo información general y no constituyen un aviso. ROOEDA se ocupará de mantener el Sitio Web y sus contenidos de forma razonablemente correcta y actualizada, pero no podrá garantizar que los contenidos del Sitio Web carezcan de errores, defectos, malware y/o virus, así como de que el Sitio Web sean correctos, estén actualizados y sean precisos. 
            ROOEDA no será responsable por ningún daño derivado del uso del (o incapacidad de usar el) Sitio Web, incluyendo los daños causados por malware, virus o cualquier información incorrecta o incompleta del contenido del Sitio Web, salvo que este daño se derive de cualquier conducta dolosa o negligencia grave por parte de ROOEDA. 
            Asimismo, ROOEDA no será responsable por los daños derivados del uso de (o incapacidad de usar) los medios de comunicación electrónicos con el Sitio Web, incluyendo sin limitación daños derivados del fallo o retraso en la entrega de comunicaciones electrónicas, intercepción o manipulación de comunicaciones electrónicas por terceros o por programas informáticos usados para comunicaciones electrónicas y transmisión de virus.

            <br></br>
            <br></br>
            <strong>
              <span>VIGENCIA Y FINALIZACIÓN DEL CONTRATO</span>
            </strong>
            <br></br>
            <br></br>

            Se subscribe el presente Contrato entre ROOEDA y usted se suscribe por un periodo indefinido.
            Usted y ROOEDA tendrán derecho a finalizar el Contrato en todo momento, siempre y cuando usted, mediante escrito libre, solicite a ROOEDA deshabilite su nombre de usuario del Sitio Web.
            ROOEDA tendrá derecho a terminar el Contrato en todo momento y con efecto inmediato (deshabilitando el uso del Servicio) si usted: ¥ Viola o incumple cualquier condición de los presentes Términos y Condiciones; y/o ¥ A consideración de ROOEDA, hace uso indebido del Sitio Web y/o de los Servicios.
            ROOEDA no estará obligado a dar un aviso previo de la terminación del Contrato. Después de su terminación, ROOEDA avisará de ello de acuerdo a lo establecido en estos Términos y Condiciones.

            <br></br>
            <br></br>
            <strong>
              <span>MODIFICACIÓN DE LOS SERVICIOS Y DE ESTOS TÉRMINOS Y CONDICIONES</span>
            </strong>
            <br></br>
            <br></br>
            
            ROOEDA se reserva el derecho, a su entera discreción, de modificar o sustituir cualquiera 
            de los presentes Términos y Condiciones del usuario, y/o cambiar, suspender o interrumpir 
            los Servicios y/o el acceso del Sitio Web (incluyendo, sin limitación, la disponibilidad 
            de cualquier característica, base de datos o contenido), en cualquier momento, mediante 
            la publicación de un aviso en el Sitio Web o a través de correo electrónico: <a href="mailto:hola@rooeda.com">hola@rooeda.com</a>

            <br></br>
            <br></br>
            <strong>
              <span>NOTIFICACIONES</span>
            </strong>
            <br></br>
            <br></br>
                        
            ROOEDA podrá emitir notificaciones o avisos a usted a través de un aviso general en el Sitio Web o mediante correo electrónico a la dirección registrada en la información de la cuenta de ROOEDA, o mediante una comunicación escrita enviada por correo ordinario a la dirección registrada en la información de la cuenta de ROOEDA.

            <br></br>
            <br></br>
            <strong>
              <span>JURISDICCIÓN Y LEY APLICABLE</span>
            </strong>
            <br></br>
            <br></br>

            Las partes están de acuerdo en que el presente Contrato se regirá por las leyes aplicables vigentes en Monterrey, Nuevo León. Para la interpretación y cumplimiento del Contrato, las partes se someten a la jurisdicción de los tribunales competentes en Monterrey, Nuevo León, renunciando expresamente a cualquier otro fuero que pudiera corresponderles por razón de sus domicilios presentes o futuros o por cualquier otra causa. 

            <br></br>
            <br></br>
            <strong>
              <span>ACEPTACIÓN DE RIESGO, RENUNCIA Y LIBERACIÓN DE RESPONSABILIDAD</span>
            </strong>
            <br></br>
            <br></br>


            El suscrito (el “Usuario”), mediante la inscripción a ROOEDA y/o asistiendo a clases, eventos, actividades y otros programas, así como el uso de las instalaciones y el equipo (“Clases” y/o “Instalaciones”) de ROOEDA, por medio de la presente, reconoce que existen ciertos riesgos y peligros inherentes al uso y práctica de cualquier ejercicio físico y en específico en este caso, a la práctica y uso de bicicletas fijas, durante las Clases que se imparten en ROOEDA.
            <br></br>
            <br></br>
            El Usuario reconoce que los riesgos específicos varían de una actividad a otra, mismos que podrían ser, sin limitación: (a) lesiones menores como: (i) rasguños; y (ii) golpes y torceduras; o (b) lesiones mayores como (i) lesiones en las articulaciones o espalda; (ii) ataques cardíacos; y/o (iii) contusiones; y/o (c) lesiones graves, incluyendo parálisis, y muerte, por lo que expresamente el Usuario reconoce y acepto que dichos riesgos no pueden ser eliminados por ROOEDA.
            <br></br>
            <br></br>
            El Usuario reconoce que ha leído y entendido completamente el reglamento interno de ROOEDA, mismo que se publican en el Sitio Web de ROOEDA (www.rooeda.com) y el cual me fue proporcionado de forma física por personal de ROOEDA. El Usuario se compromete a cumplir con todos los términos y condiciones establecidos en dichos documentos, así como las instrucciones que de tiempo en tiempo el personal de ROOEDA me proporcione durante el desarrollo de las Clases, o en su caso, con las instrucciones que ROOEDA ponga en el establecimiento donde se lleven a cabo las Clases.

            Si en cualquier momento, el personal de ROOEDA me sugiere y me indica que no podré llevar a cabo cualesquiera de las Clases que ROOEDA imparte acataré dicha instrucción. Lo anterior, basado en la opinión del personal de ROOEDA, quienes reconozco están debidamente capacitados para emitir dicha opinión, por lo que entiendo y acepto que dicha opinión siempre será en mi beneficio y en cuidado de mi salud y mi persona. 
            <br></br>
            <br></br>
            En relación con lo anterior, en caso que ROOEDA me permita tomar las Clases (i) asumo plena responsabilidad por cualquier y todas las lesiones o daños que sufra (incluso muerte) durante o derivado de las Clases, (ii) libero a ROOEDA y sus subsidiarias, y cada uno de sus socios, accionistas, consejeros, funcionarios, directores, empleados, representantes y/o agentes, y a cada uno de sus respectivos sucesores y cesionarios de cualquier y toda responsabilidad, reclamaciones, acciones, demandas, procedimientos, costos, gastos, daños y perjuicios que se pudieran causar; y (iii) manifiesto que, a esta fecha, (a) no tengo ningún impedimento médico o condición física que me impida tomar las Clases o usar correctamente los aparatos mediante los cual se llevan a cabo las Clases; (b) no tengo una condición física o mental que me ponga peligro médico y físico; y (c) no tengo instrucciones médicas que me limiten o restrinjan realizar cualquier tipo de actividad física.
            <br></br>
            <br></br>
            Reconozco que en caso de que el Usuario tenga alguna discapacidad o enfermedad crónica, está en riesgo al hacer uso de las instalaciones y acudir a las Clases, y que no debería de participar en cualquier Clase.
            He leído esta declaratoria de aceptación de riesgo, renuncia y liberación de responsabilidad, y deslindo de toda responsabilidad, obligándome a sacar en paz y a salvo a ROOEDA y/o a todas sus subsidiaras, y a cada uno de sus socios, accionistas, consejeros, funcionarios, directores, empleados representantes y/o agentes respecto de toda acción, demanda, responsabilidad de carácter civil o penal derivado de cualquier contingencia, accidente, daño, o cualquier tipo de lesión, enfermedad, fracturas, incapacidad parcial o permanente y/o la muerte que pudiera sufrir el Usuario por el uso de las Instalaciones ROOEDA y/o por las Clases que tome.
            Reconozco que estoy firmando el presente de manera libre y voluntariamente y que la vigencia de esta renuncia es indefinida por lo que continuará valida y vigente durante el tiempo que acuda a las Instalaciones y/o tome clases de ROOEDA.
            VALORES Y BIENES PERSONALES

            ROOEDA no será responsable por la pérdida, robo, o daños a cualquier objeto, incluyendo artículos dejados en casilleros, baños, estudios, o cualquier otro lugar en las instalaciones. 
            <br></br>
            <br></br>
            Asimismo, acepto y reconozco que ROOEDA se reserva el derecho a denegar el acceso a cualquier persona que ROOEDA considere que esté actuando de manera inadecuada o que pongan en riesgo su salud o la salud de los clientes
            </p>
            <br></br>
            <br></br>
            <br></br>
            <p>
                <strong><span>Última actualización:</span> </strong>15 de Junio de 2020  
            </p>
            <br></br>
            <br></br>
            <br></br>
          </div>
          <div className="bx--col-sm-8 bx--col-lg-6"></div>
          
          </div>



      </div>
    </div>

    <Footer />
    </>
  );
};
export default TermsAndConditions;
