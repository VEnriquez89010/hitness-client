import React from 'react';
import Footer from '../../components/Footer';
import ls from 'local-storage'
import { Button , OverflowMenu, OverflowMenuItem } from 'carbon-components-react';
import { Modal } from 'react-responsive-modal';
import axios from 'axios';
import { MisuseOutline32  , TrashCan32} from '@carbon/icons-react';
import appSettings from '../../helpers/AppSettings';
import { ToastContainer, toast } from 'react-toastify';

class Profile extends React.Component {
  constructor(props) {
    super(props);

    let session = ls.get('session');
   
    if (!session) {
      window.location.href='/login';
      return;
    }

    this.state = {
      session: session,
      name: ls.get('name'),
      clases: 0,
      open: false,
      expire: '',
      data: [],
      classRooms: [],
      classRoomId: '',
      packId: ''
    };
  }

  getInfo(){
    axios.all([
      axios.get(`${appSettings.SERVER_URL}/user-pack/available/${this.state.session}`),
      axios.get(`${appSettings.SERVER_URL}/user-schedule/id/${this.state.session}`),
      axios.get(`${appSettings.SERVER_URL}/class-room/all`),
    ])
      .then(axios.spread((pack, schedule, classRooms) => {
        if(pack.status === 200){
          this.setState({clases: pack.data.Clases, expire: pack.data.Expires, packId: pack.data.PackId});
        }

        if(schedule.status === 200){
          this.setState({data: schedule.data});
        }

        if(classRooms.status === 200){
          this.setState({classRooms: classRooms.data});
        }
      })).catch(error => this.setState({ error, isLoading: false }));
  }

  componentDidMount() {
    this.getInfo();
  }

  setClassRoomName(classRoomId){
    let result =  this.state.classRooms.find(x => x._id === classRoomId);
    return (result) ? result.Name: null;
  }

  setClassRoomId(classRoomId){
    this.setState({open: true, classRoomId: classRoomId});
  }

  delete(schedule){
    axios.all([
      axios.delete(`${appSettings.SERVER_URL}/user-schedule/${schedule.UserScheduleId}`),
      axios.put(`${appSettings.SERVER_URL}/user-pack/add-class/${this.state.packId}`)
    ]).then(axios.spread((schedule, pack) => {
      this.setState({open: false});
      toast('Reservacion eliminada satisfactoriamente');
      this.getInfo();
    })).catch(error => this.setState({ error, isLoading: false }));
  }

  onCloseModal = () => {
    this.setState({ open: false });
  };

  datediff() {
    var now = new Date();
    var today = Date.UTC(now.getFullYear(), now.getUTCMonth(), now.getDate())
    var expire = new Date(this.state.expire);
    return Math.round((expire-today)/(1000*60*60*24));
  }

  render() {
    const { open } = this.state;
    return (
              <>
              <div className="profileStyle">
                <div className="bx--grid">
                  <div className="bx--row">
                  <div className="bx--col-md-2"></div>
                  <div className="bx--col-md-4">
                      <div className="centerTitle">
                          <p>Hola</p>
                          <h3>{this.state.name}</h3>
                      </div>
                      <div className="profileContainer">
                        <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" data-type='profile'>
                          <div className="bx--aspect-ratio--object">
                            <div className="packContent">
                              <h2>{this.state.clases}</h2>
                              <h4>clases</h4>
                            <div className="packContentBottom" data-type='expiration'>
                              {(this.state.expire) ?
                              <p>Expira en <span>{this.datediff()}</span> días</p>
                              : null }
                            </div>
                            </div>
                          </div>
                        </div>
                        {(this.state.clases > 0) ? 
                        <Button type="submit" value="Submit" className="buttonAccess" onClick={()=> window.location.href='/calendar'}>
                        Agendar clase
                        </Button>
                        : 
                        <Button type="submit" value="Submit" className="buttonAccess" onClick={()=> window.location.href='/packs'}>
                          Comprar packs
                        </Button>
                        }
                      </div>
                  </div>
                  <div className="bx--col-md-2"></div>
                  </div>
                  
                  {(this.state.data.length > 0)
                    ?
                    <>
                    <div className="bx--row">
                    <div className="bx--col-md-2"></div>
                    <div className="bx--col-md-4">
                      <br></br>
                      <br></br>
                      <h4>Reservaciones</h4>
                    </div>
                    <div className="bx--col-md-2"></div>
                    </div>

                    <div className="bx--row">
                    <div className="bx--col-md-2"></div>
                    <div className="bx--col-md-4 trainerRow">
                      <p data-type='title'>Salón ----- </p>
                      <p data-type='title'>Horario</p>
                      <p className="bikeNumber"  data-type='title'>Bicicleta</p>
                      <p className="bikeNumber"  data-type='title'></p>
                    </div>
                    <div className="bx--col-md-2"></div>
                    </div>
                    </>
                    :
                    <div>
                      <br/>
                      <h4>No hay reservaciones</h4>
                    </div>
                  }

                  { this.state.data.map( (element, index) => {
                    return(
                            <div key={index}  className="bx--row">
                            <div className="bx--col-md-2"></div>
                            <div className="bx--col-md-4 trainerRow">
                              <p>{(element.ClassRoomId) ? this.setClassRoomName(element.ClassRoomId) : null} ----- </p>
                              <p>{element.ModifiedTime} - {element.Day} {element.Date} de {element.Month}</p>
                              <p className="bikeNumber">{element.Bike}</p>
                              <OverflowMenu className="overflowMenu" flipped>
                                <OverflowMenuItem 
                                    itemText="Cancelar" 
                                    hasDivider 
                                    isDelete 
                                    onClick={() => this.setClassRoomId(element)}
                                />
                              </OverflowMenu>
                            </div>
                            <div className="bx--col-md-2"></div>
                            </div>
                      );
                    })
                  }
                </div>
              </div>
              <Footer />
              <Modal 
              open={open} 
              onClose={this.onCloseModal} 
              center
              animationDuration={80}
              >

             <p>¿Realmente deseas cancelar tu reservacion?</p>
             <br></br>
             <Button 
                renderIcon={MisuseOutline32 }  
                type="submit" 
                value="Submit" 
                size='small'
                kind='ghost' 
                onClick={this.onCloseModal}
              >
                Cancelar
              </Button>
              <Button 
                renderIcon={TrashCan32}  
                type="submit" 
                value="Submit" 
                size='small'
                kind='ghost' 
                onClick={() => this.delete(this.state.classRoomId)}
              >
                Eliminar
              </Button>
            </Modal>
            <ToastContainer 
            autoClose={3000}
          />
              </>
            );
};
}

export default Profile;
