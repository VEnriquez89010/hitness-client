import React from 'react';
import { Link } from 'react-router-dom';
import '../../components/LoginControl/LoginControl.scss';
import { Form, TextInput, Button } from 'carbon-components-react';
import ls from 'local-storage'
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const NameInputProps = {
  className: 'name',
  id: 'name',
  labelText: 'Nombre',
  placeholder: ''
};
const EmailInputProps = {
  className: 'age',
  id: 'email',
  labelText: 'Correo electrónico',
  placeholder: ''
};

const PasswordInputProps = {
  className: 'password',
  id: 'pass',
  labelText: 'Contraseña',
  placeholder: 'Mínimo 6 caracteres + 1 dígito',
  invalid: false,
  invalidText:
    'Your password must be at least 6 characters as well as contain at least one uppercase, one lowercase, and one number.',
};


const buttonEvents = { className: 'buttonAccess' };

class SignupForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {name: '', email: '', password: '', buttonDisabled: false};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  notify = () => toast("Correo electrónico registrado. Por favor prueba otro diferente.");

  handleChange(event) {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({buttonDisabled: true});
    let values =  { Name: this.state.name, Email: this.state.email, Password: this.state.password }  // { Email: obtenEvent }
    axios.post(`${appSettings.SERVER_URL}/users/validate-mail`, { Email:this.state.email})
    .then( existEmail => {
      if(existEmail.status === 200){
        if(!existEmail.data.Available){
          //--- Toast Failed. ya existe correo electronico
          this.notify()
          this.setState({buttonDisabled: false});
          return;
        }else{
          axios.post(`${appSettings.SERVER_URL}/users/add`, values)
          .then(function (response) {
              
            let userID   = response.data.user.id;
            let userName = response.data.user.Name;
      
            ls.set('session', userID);
            ls.set('name', userName);
            
            window.location.href='/profile';
          }).catch( () => {
            //--- Toeaat error, no se creo usuario
            this.setState({buttonDisabled: false});
          });
        }
      }
    }).catch(error => {
       //--- Toast fail. Email error
       this.setState({buttonDisabled: false});
    });
  }

  render() {

        /*  Facebook and Google URL redirection */
         function facebookSignup(e) {
          e.preventDefault();
            window.location.href='/fb/loginFB';
        }
         function googleSignup(e) {
          e.preventDefault();
          window.location.href='/google';
        }
    

    return (
      
      <div className="formStyle">
            <ToastContainer 
            position="top-right"
            autoClose={false}
            />
      <div className="bx--grid">
      <div className="bx--row">
        <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
        <div className="bx--col-lg-4 bx--col-md-4 bx--col-sm-4">
          <Form onSubmit={this.handleSubmit}>

              <h3 className="formTitles">
                Crear cuenta
              </h3>

              <TextInput
              name='name'
              type="text" 
              required
              value={this.state.value} 
              onChange={this.handleChange} 
              
              {...NameInputProps}  
              />


              <TextInput
              name='email'
              type="email" 
              required
              value={this.state.value} 
              onChange={this.handleChange}
              pattern="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+"
              
              {...EmailInputProps}  
              />

              <TextInput
              name='password'
              type="password" 
              required
              value={this.state.value} 
              onChange={this.handleChange} 
              pattern="(?=.*\d)(?=.*[a-z]).{7,}"

              {...PasswordInputProps}  
              />

              <Button type="submit" value="Submit" className="buttonAccess" disabled={this.state.buttonDisabled}>Registrarse</Button>

                  <div className="buttonGoogle">
                  <Button onClick={googleSignup} type="submit" {...buttonEvents}>   </Button>
                  </div>

                  <div className="buttonFacebook">
                  <Button onClick={facebookSignup} type="submit" {...buttonEvents}> </Button>
                  </div>
          </Form>

          <div className="alignBottom" data-type='privacyPolicy'>
          <div className="privacyPolicy">Al continuar, aceptas las
              <Link element={Link} to="/terms-and-conditions" className="privacyPolicy">
                Condiciones de uso
              </Link>
                y el 
              <Link element={Link} to="/privacy-policy" className="privacyPolicy">
                Aviso de privacidad
              </Link>
                de Hitness Workout Studio.
          </div>
          </div>

        </div>  
        <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
      </div>
      </div>
      </div>
    );
  }
}

export default SignupForm;