import React from 'react';
import { Link } from 'react-router-dom';
import '../../components/LoginControl/LoginControl.scss';
import { Form, TextInput, Button } from 'carbon-components-react';
import ls from 'local-storage'
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const EmailInputProps = {
  className: 'age',
  id: 'email',
  labelText: 'Correo electrónico',
  placeholder: ''
};

const PasswordInputProps = {
  className: 'password',
  id: 'pass',
  labelText: 'Contraseña',
  placeholder: ''
};

const buttonEvents = { className: 'buttonAccess' };

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {email: '', password: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  
  handleChange(event) {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }

  handleSubmit(event) {
    event.preventDefault();
    let values =  { Email: this.state.email, Password: this.state.password } 
    axios.post(`${appSettings.SERVER_URL}/sessions/login`, values)
    .then(function (response) {
        if(response.status === 200){
          let userID   = response.data.user.id;
          let userName = response.data.user.Name;

          ls.set('session', userID);
          ls.set('name', userName);
          
          window.location.href='/profile';
        }else{
          //---- Toastr error, not found
          toast("El usuario no existe.");
          return;
        }
    })
    .catch(function (error) {
      //---- Toastr error, not found
      toast("Correo electrónico o contraseña invalidos.");
    });

  }


  render() {

        /*  Facebook and Google URL redirection */
         function facebookSignup(e) {
          e.preventDefault();
            window.location.href= `${appSettings.SERVER_URL}/fb/loginFB`;
        }
         function googleSignup(e) {
          e.preventDefault();
          window.location.href=`${appSettings.SERVER_URL}/google`;
        }
    

    return (
      <>
      <ToastContainer 
      position="top-right"
      autoClose={3000}
      />
      <div className="formStyle">
      <div className="bx--grid">
      <div className="bx--row">
        <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
        <div className="bx--col-lg-4 bx--col-md-4 bx--col-sm-4">
          <Form onSubmit={this.handleSubmit}>

              <h3 className="formTitles">
                Iniciar sesión
              </h3>

              <TextInput
              name='email'
              type="email" 
              required
              value={this.state.value} 
              onChange={this.handleChange} 
              
              {...EmailInputProps}  
              />

              <TextInput
              name='password'
              type="password" 
              required
              value={this.state.value} 
              onChange={this.handleChange} 
              
              {...PasswordInputProps}  
              />

              <Button type="submit" value="Submit" className="buttonAccess">Iniciar sesión</Button>

                  <div className="buttonGoogle">
                  <Button onClick={googleSignup} type="submit" {...buttonEvents}>   </Button>
                  </div>

                  <div className="buttonFacebook">
                  <Button onClick={facebookSignup} type="submit" {...buttonEvents}> </Button>
                  </div>

               <Link className="forgotPass" element={Link} to="/forgotpass">
                ¿Olvidaste tu contraseña?
              </Link>
          </Form>

          <div className="alignBottom" data-type='privacyPolicy'>
          <div className="privacyPolicy">Al continuar, aceptas las
              <Link element={Link} to="/terms-and-conditions" className="privacyPolicy">
                Condiciones de uso
              </Link>
                y el 
              <Link element={Link} to="/privacy-policy" className="privacyPolicy">
                Aviso de privacidad
              </Link>
                de Hitness Workout Studio.
          </div>
          </div>

        </div>  
        <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
      </div>
      </div>
      </div>
      </>
    );
  }
}

export default LoginForm;