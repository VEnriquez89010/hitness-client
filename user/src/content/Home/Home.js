import React from 'react';
import './Home.scss?v=2.0.0';
import IconFacebook from './icon-facebook.svg';
import IconInstagram from './icon-instagram.svg';
import { Link } from 'react-router-dom';
import { Button } from 'carbon-components-react';
import { ChevronRight32, Flash32, Launch16 } from '@carbon/icons-react';
import Footer from '../../components/Footer';
import Packs from '../../components/Packs';
import ls from 'local-storage'


const renderAuthButton = ()=>{
  let session = ls.get('session');
  if(session == null){
    return  <Link element={Link} to="/signup">
              <Button renderIcon={Flash32}>Crea tu cuenta</Button>
            </Link>
  } else{
    return <Link element={Link} to="/packs">
              <Button renderIcon={Flash32}>Crea tu cuenta</Button>
           </Link>
  }
}

const Home = () => {
  return (
    <>
    <div className="heroHome">
      <div className="bx--grid bx--grid--full-width landing-page">
        <div className="heroHeadline">
        <div className="bx--col-md-2"></div>
        <div className="bx--col-lg-8">
          <div className="alignBottom" data-type='home'>
            <h1 >Somos un indoor cycling studio.</h1>
            <br></br>
            {renderAuthButton()}
            <ul className="socialNetworks" data-type='heroHome'>
                  <li>
                    <a href="https://www.instagram.com/rooedastudio/">
                      <img src={IconInstagram} alt='Rooeda™ Studio' />  
                    </a>
                  </li>
                  <li>
                    <a href="https://facebook.com">
                      <img src={IconFacebook} alt='Rooeda™ Studio' />  
                    </a>
                  </li>
                </ul>
        </div>
        </div>
        <div className="bx--col-md-2"></div>
        </div>
    </div>

    </div>
    
    <div className="aboutHome">
    <div className="bx--grid">
    <div className="bx--row">
        <div className="bx--col-md-2"></div>
        <div className="bx--col-lg-8">
            <h1>
              <strong> Somos una familia</strong> que <strong>rooeda</strong> 
              en comunidad hacia el bienestar físico.
            </h1>
            <br></br>
            <br></br>
            <p>
            <strong>Rooeda™</strong> es una experiencia, buscamos inspirarte, ponerte retos y llevarte hacia adelante.  
            A retarte a ser mejor, no sólo en la bici sino en tu vida.  
            Queremos ser la mejor parte de tus días. 
            </p>
            <br></br>
            <br></br>
            <br></br>
            <br></br>
            <br></br>
            <Link element={Link} to="/instructors" className="spotifyButton">
              Playlist mensual en Spotify
              <Launch16 className="spotifyIcon"/>
            </Link>
        </div>
        <div className="bx--col-md-2"></div>
    </div>
    </div>
    </div>
    <Packs />
    <Footer />
    </>
  );
};
export default Home;
