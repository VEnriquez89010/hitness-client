import React from 'react';
import { OverflowMenu, OverflowMenuItem } from 'carbon-components-react';

const Overflow = () => {

    return (
    <OverflowMenu className="overflowMenu" flipped>
    <OverflowMenuItem itemText="Editar" />
    <OverflowMenuItem itemText="Eliminar" hasDivider isDelete/>
    </OverflowMenu>
    );
  };
  export default Overflow;
  