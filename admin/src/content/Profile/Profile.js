import React from 'react';
import './Profile.scss';


const Home = () => {
  return (
<>

    <div className="bx--grid bx--grid--full-width landing-page">
    {/* <aside class="bx--side-nav sideBar"></aside> */}
    <div className="bx--row landing-page__banner">
    <div className="bx--col-lg-16">
      <h1 className="landing-page__heading paddingTitle">
        Hello Paola
      </h1>
  </div>
</div>


    <div className="bx--row landing-page__r3">
      <div className="bx--col-md-4 bx--col-lg-3 bx--no-gutter">
      <div className="bx--aspect-ratio bx--aspect-ratio--1x1 borderTop ">
          <div className="bx--aspect-ratio--object">15 Members</div>
      </div>
      </div>
      <div className="bx--col-md-4 bx--col-lg-3 bx--no-gutter">
      <div className="bx--aspect-ratio bx--aspect-ratio--1x1 borderTop borderLeft">
          <div className="bx--aspect-ratio--object">8 Trainers</div>
      </div>
      </div>
      <div className="bx--col-md-4 bx--col-lg-3 bx--no-gutter">
      <div className="bx--aspect-ratio bx--aspect-ratio--1x1 borderTop borderLeft">
          <div className="bx--aspect-ratio--object">22 Classes</div>
      </div>
      </div>
      <div className="bx--col-md-4 bx--col-lg-3 bx--no-gutter">
      <div className="bx--aspect-ratio bx--aspect-ratio--1x1 borderTop borderLeft">
          <div className="bx--aspect-ratio--object">30 Playlists</div>
      </div>
      </div>
    </div>
  </div>
  </>
  );
};
export default Home;
