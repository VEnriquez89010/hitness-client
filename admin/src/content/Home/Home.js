import React from 'react';
import './Home.scss?v=2.0.0';
import { 
  Cyclist32, 
  EventSchedule32, 
  Events32, 
  AddAlt32 } from '@carbon/icons-react';
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';

  class Home extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        Instructors: '',
        Members: '',
        Schedules: ''
      };
  
      this.componentDidMount = this.componentDidMount.bind(this);
    }

    componentDidMount() {
      let currentState = this;
      axios.all([
        axios.get(`${appSettings.SERVER_URL}/dashboard`),
        axios.get(`${appSettings.SERVER_URL}/user-pack/all-available`)
      ]).then(axios.spread((dashboard, packs) => {
          dashboard.data.members = packs.data.length;
          let { members, instructors, schedules } =  dashboard.data;
          currentState.setState({Instructors: instructors, Members: members, Schedules: schedules});
      })).catch(error => this.setState({ error, isLoading: false }));
    }

    handleSubmit(){
      window.location.href = '/signup';
    }

    render(){
      let { Members, Instructors, Schedules } =  this.state;
      return (
      <>
                <div className="dashBoard" data-type='home'>
                <div class="bx--grid bx--grid--full-width landing-page">
                <div className="bx--row">
                    <div className="bx--col-md-4 bx--col-lg-3">
                        <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox"
                             onClick={event =>  window.location.href='/instructors'}
                        >
                        <div className="bx--aspect-ratio--object">
                        <div className="packContent">
                          <h2>{Instructors}</h2>
                          <h4>Instructores</h4>
                          <div className="packContentBottom">
                            <Cyclist32 className="BoxIcon" />
                          </div>
                          </div>
                        </div>  
                        </div> 
                    </div>
                    <div className="bx--col-md-4 bx--col-lg-3">
                        <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox"
                             onClick={event =>  window.location.href='/calendar'}
                        >
                        <div className="bx--aspect-ratio--object">
                        <div className="packContent">
                          <h2>{Schedules}</h2>
                          <h4>Clases</h4>
                          <div className="packContentBottom">
                            <EventSchedule32 className="BoxIcon" />
                          </div>
                        </div>
                        </div>  
                        </div> 
                    </div>
                    <div className="bx--col-md-4 bx--col-lg-3">
                        <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox"
                             onClick={event =>  window.location.href='/members'}
                        >
                        <div className="bx--aspect-ratio--object">
                        <div className="packContent">
                          <h2>{Members}</h2>
                          <h4>Miembros</h4>
                          <div className="packContentBottom">
                              <Events32 className="BoxIcon" />
                          </div>
                        </div>
                        </div>  
                        </div> 
                    </div>
                    <div className="bx--col-md-4 bx--col-lg-3">
                        <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" 
                             onClick={this.handleSubmit}
                        >
                        <div className="bx--aspect-ratio--object">
                        <div className="packContent">
                                      <h4>Agregar cuenta</h4>
                                    <div className="packContentBottom">
                                        <AddAlt32 className="BoxIcon" />
                                    </div>
                                    </div>
                        </div>  
                        </div> 
                    </div>
            </div>
            </div>
            </div>
      </>
      );
    };
  }

export default Home;
