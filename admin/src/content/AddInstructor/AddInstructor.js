import React from 'react';
import '../../components/LoginControl/LoginControl.scss?v=2.0.0';
import './AddInstructor.scss';
import { 
  Form, 
  TextInput, 
  Button, 
  TextArea, 
  FileUploader
 } from 'carbon-components-react';
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const NameInputProps = {
  className: 'name',
  id: 'name',
  labelText: 'Nombre',
  placeholder: ''
};
const EmailInputProps = {
  className: 'age',
  id: 'email',
  labelText: 'Música',
  placeholder: ''
};


class AddInstructorForm extends React.Component {

  constructor(props) {
    super(props);
    

    let t = props.location.pathname.split('/');
    var id = '';
    if(t[2]){
      id = t.pop();
    }

    this.state = {selectedFile: null }
    this.state = {name: '', music: '', description: '', instructorId: id, imageUrl: ''};

    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    if(this.state.instructorId){
      axios.get(`${appSettings.SERVER_URL}/instructor/id/${this.state.instructorId}`)
      .then(instructor =>{
        let { Description, Music, Name, ImagePath } = instructor.data;
        this.setState({ name: Name, music: Music, description: Description, imageUrl: ImagePath })
      }  )
      .catch(error => this.setState({ error, isLoading: false }));
    }
  }

  onChangeHandler=event=> {
    this.setState({
      selectedFile: event.target.files[0],
      loaded: 0,
    })

  }

  onClickHandler = (e) => {
    e.preventDefault();
    let values =  { Name: this.state.name, Music: this.state.music, Description: this.state.description } 
    var data = new FormData()
    data.append('instructor', JSON.stringify(values));
    data.append('file', this.state.selectedFile);
    
    axios.put(`${appSettings.SERVER_URL}/instructor/${this.state.instructorId}`, data)
    .then(() => {
      toast("Instructor agregado");
      setTimeout(() => {
        window.location.href = '/instructors'
      }, 4000);
    }).catch();
  }

  handleChange(event) {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }

  render() {
    return (
      <div className="dashBoard">
      <div className="formStyle" data-type='AddInstructor'>
            <ToastContainer 
            position="top-right"
            autoClose={4000}
            />
      <div className="bx--grid">
      <div className="bx--row">
        <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
        <div className="bx--col-lg-4 bx--col-md-4 bx--col-sm-4">
          <Form>

              <h3 className="formTitles fileUploaderLabel">
                {(!this.state.instructorId) 
                ? 'Agregar'
                : 'Modificar'} instructor
              </h3>

              {(this.state.imageUrl) ?
              <img src={this.state.imageUrl} alt=""></img>
              : null }
              
              <div className="bx--file__container">
                <FileUploader
                  accept={[
                    '.jpg',
                    '.png'
                  ]}
                  buttonKind="secondary"
                  buttonLabel="Subir foto"
                  filenameStatus="edit"
                  iconDescription="Clear file"
                  labelDescription="Sólo archivos .jpg .png  de 50mb o menos."
                  labelTitle="Foto de perfil"
                  onChange={this.onChangeHandler}
                />
              </div>
              <TextInput
              name='name'
              type="text" 
              value={this.state.name} 
              onChange={this.handleChange} 
              {...NameInputProps}  
              />


              <TextInput
              name='music'
              type="text" 
              value={this.state.music} 
              onChange={this.handleChange} 
              
              {...EmailInputProps}  
              />

             <TextArea
                  name='description'
                  value={this.state.description} 
                  onChange={this.handleChange} 
                  cols={50}
                  id="test2"
                  invalidText="Invalid error message."
                  labelText="Biografía"
                  placeholder=""
                  row={4}
                />
              <br></br>
              <Button 
                  type="submit" 
                  value="Submit" 
                  className="buttonAccess"
                  size="small"
                  kind="secondary"
                  onClick={this.onClickHandler}
              >
                  Guardar
              </Button>

          </Form>

        </div>  
        <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
      </div>
      </div>
      </div>
      </div>
    );
  }
}

export default AddInstructorForm;