import React from 'react';
import '../../components/LoginControl/LoginControl.scss';
import { Form, TextInput, Button, FormGroup } from 'carbon-components-react';
import appSettings from '../../helpers/AppSettings';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const marginForm = { marginBottom: '0.8rem' };

const NewPasswordProps = {
  classNameName: 'some-className',
  id: 'test3',
  labelText: 'Contraseña',
  invalid: false,
  invalidText: '6 letters + 2 digits.',
  placeholder: 'Mínimo 6 caracteres + 1 dígito',
};

const buttonEvents = {
  className: 'buttonAccess'
};

class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      password: '',
      id: props.location.pathname.split('/').pop()
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event){
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }

  handleSubmit(event){
    event.preventDefault();
    let {id, password} = this.state;

    axios.post(`${appSettings.SERVER_URL}/users/restore-password`, {Id: id, Password: password})
    .then(function (response) {
      if(response.status === 200){
        //--- cambio de contraseña exitoso, redirigir a algun lugar, login en este caso
        toast("Cambio de contraseña exitoso");
        setTimeout(() => {
          window.location.href='/login'
          }, 4000);
      } else {
        // --- fallo el cambio de contraseña, mostrar un toast de error (id invalido)
        toast("Error");
      }
    }).catch();
  }

render() {
    return (
      <>
      <ToastContainer 
        position="top-right"
        autoClose={3000}
      />
      <div className="formStyle">
        <div className="bx--grid">
        <div className="bx--row">
            <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
            <div className="bx--col-lg-4 bx--col-md-4 bx--col-sm-4">
              <h3 className="formTitles">
                Nueva contraseña
              </h3>
              <Form>
              <FormGroup>
                <TextInput
                  style={marginForm}
                  type="password"
                  name='password'
                  value={this.state.value} 
                  required
                  pattern="(?=.*\d)(?=.*[a-z]).{7,}"

                  {...NewPasswordProps} 
                  onChange={this.handleChange}
                />
  
                <Button type="submit"{...buttonEvents} onClick={this.handleSubmit}>
                  Confirmar
                </Button>
              </FormGroup>
              </Form>

              <div className="alignBottom" data-type='privacyPolicy'>
              <div className="privacyPolicy">
                2020 Rooeda™ Studio.
              </div>
              </div>


            </div>
            <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
        </div>
        </div>
      </div>
      </>
    );
  };
}

export default Login;
