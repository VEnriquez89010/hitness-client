import React from 'react';
import './Reservations.scss?v=2.0.0';
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import ls from 'local-storage'

class Reservations extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      clases: 0,
      expire: '',
      data: [],
      classRooms: [],
      id: props.location.pathname.split('/').pop()
    };
  }

  componentDidMount() {
    axios.all([
      axios.get(`${appSettings.SERVER_URL}/user-pack/available/${this.state.id}`),
      axios.get(`${appSettings.SERVER_URL}/user-schedule/id/${this.state.id}`),
      axios.get(`${appSettings.SERVER_URL}/class-room/all`)
    ])
      .then(axios.spread((pack, schedule, classRooms) => {
        if(pack.status === 200){
          this.setState({clases: pack.data.Clases, expire: pack.data.Expires});
        }

        if(schedule.status === 200){
          this.setState({data: schedule.data});
        }

        if(classRooms.status === 200){
          this.setState({classRooms: classRooms.data});
        }
        this.setState({name: ls.get('memberName')})
      })).catch(error => this.setState({ error, isLoading: false }));
  }

  setClassRoomName(classRoomId){
    let result =  this.state.classRooms.find(x => x._id === classRoomId);
    return (result) ? result.Name: null;
  }

  render(){
    return (
      <div className="dashBoard">
      <div className="formStyle">
        <div className="bx--grid">
          <div className="bx--row">
            <div className="bx--col-lg-4"></div>
            <div className="bx--col-lg-4 centerTitle">
              <h1>Reservaciones</h1>
            </div>
            <div className="bx--col-lg-4"></div>
          </div>

          <div className="bx--row">
                    <div className="bx--col-md-2"></div>
                    <div className="bx--col-md-4 trainerRow">
                      <p>
                        <strong>
                          {this.state.name}
                        </strong>
                        </p>
                    </div>
                    <div className="bx--col-md-2"></div>
          </div>

          {(this.state.data.length > 0)
                    ? 
                    <div className="bx--row">
                      <div className="bx--col-md-2"></div>
                      <div className="bx--col-md-4 trainerRow">
                        <p data-type='title'>Salón ------ </p>
                        <p data-type='title'>Horario</p>
                        <p className="bikeNumber"  data-type='title'>Bicicleta</p>
                      </div>
                      <div className="bx--col-md-2"></div>
                    </div>
                    :
                    <div className="bx--row">
                      <div className="bx--col-md-2"></div>
                      <div className="bx--col-md-4 trainerRow">
                        <p data-type='title'>No hay reservaciones</p>
                      </div> 
                    </div>
                  }

          { this.state.data.map( (element, index) => {
                    return(
                            <div key={index}  className="bx--row">
                            <div className="bx--col-md-2"></div>
                            <div className="bx--col-md-4 trainerRow">
                              <p>{(element.ClassRoomId) ? this.setClassRoomName(element.ClassRoomId) : null} ------ </p>
                              <p>{element.ModifiedTime} - {element.Day} {element.Date} de {element.Month}</p>
                              <p className="bikeNumber">{element.Bike}</p>
                            </div>
                            <div className="bx--col-md-2"></div>
                            </div>
                      );
                    })
                  }
        </div>
      </div>
      </div>
    );
  };
}


export default Reservations;
