import React from 'react';
import './Members.scss?v=2.0.0';
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import ls from 'local-storage'

class Members extends React.Component {
  constructor(props) {
    super(props);
    this.state = {data: []};
  }

  componentDidMount() {
      axios.get(`${appSettings.SERVER_URL}/user-pack/all-available`)
      .then(packs =>  this.setState({ data: packs.data }))
      .catch(error => this.setState({ error, isLoading: false }));
  }

  datediff(ex) {
    var now = new Date();
    var today = Date.UTC(now.getFullYear(), now.getUTCMonth(), now.getDate())
    var expire = new Date(ex);
    return Math.round((expire-today)/(1000*60*60*24));
  }

  render(){
    return (
      <div className="dashBoard">
      <div className="formStyle">
        <div className="bx--grid">
          <div className="bx--row">
            <div className="bx--col-lg-4"></div>
            <div className="bx--col-lg-4 centerTitle">
              <h1>Miembros</h1>
            </div>
            <div className="bx--col-lg-4"></div>
          </div>

          <div class="bx--row">
                    <div class="bx--col-md-2"></div>
                    <div class="bx--col-md-4 trainerRow">
                      <p data-type='title'>
                          Nombre
                      </p>
                      <p className="bikeNumber"  data-type='title'>
                          Expiración
                      </p>
                    </div>
                    <div class="bx--col-md-2"></div>
          </div>
          
          { this.state.data.map( (element, index) => {
                return(
                  <div key={index} className="bx--row">
                  <div className="bx--col-md-2"></div>
                  <div className="bx--col-md-4 trainerRow" >
                    <p
                      className="instructorsLink"
                      onClick={ function profile () {
                      ls.set('memberName', element.Name );
                      window.location.href=`/reservations/${element.Id}`;
                      }}
                    >{element.Name}</p>
                    { ( this.datediff(element.Expire) > 0 )
                    ?
                    <p className="expire">{this.datediff(element.Expire)} {( this.datediff(element.Expire) === 1 ) ? 'día' : 'días'  }</p>
                    : 
                    <p className="expire">Expirado</p>
                    }
                    {/* 
                      <OverflowMenu className="overflowMenu" flipped>
                      <OverflowMenuItem 
                        itemText="Reservaciones" 
                        onClick={ function profile () {
                          ls.set('memberName', element.Name );
                          window.location.href=`/reservations/${element.Id}`;
                          }}
                      />
                      </OverflowMenu> 
                    */}
                  </div>
                  <div className="bx--col-md-2"></div>
                  </div>
                );
              })
          }
        </div>
      </div>
      </div>
    );
  };
}


export default Members;
