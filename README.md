# Hitness-Client



## How to Run

>>>
1. Install mongo service
  * https://docs.mongodb.com/manual/installation/



2. Clone project
  * git clone https://gitlab.com/VEnriquez89010/hitness-client.git



3. Run Admin project
  * cd hitness-client/admin
  * npm install
  * npm start
  * Listen Admin on port 3006
    * http://localhost:3006



4. Run User project
  * cd hitness-client/user
  * npm install
  * npm start
  * Listen User on port 3000
    * http://localhost:3000
>>>
